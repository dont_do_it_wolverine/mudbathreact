import React, { Component } from 'react';
import "./Navigation.css";

class Navigation extends Component {
    render() {
        return (
            <div className="Navigation">
                <div className="Navigation_left">
                    {this.props.left}
                </div>
                <div className="Navigation_right">
                    {this.props.right}
                </div>
            </div>
        );
    }
}

export default Navigation;