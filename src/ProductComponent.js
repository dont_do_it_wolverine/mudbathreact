import React, { Component } from 'react';

/**
 * Higher order component to handle currency conversions
 * @param {any} ProductComponent
 */
const productComponent = (ProductComponent) => {
    return class extends Component {
        convertPrice() {
            let convPrice, exchangeRate;

            if (this.props.product.price.base === this.props.selectedCurrency.base) {
                convPrice = this.props.product.price.amount;
            } else {
                exchangeRate = this.props.currencies.filter((v) => v.base === this.props.selectedCurrency.base)[0];
                convPrice = (this.props.product.price.amount/exchangeRate.rates[this.props.product.price.base]).toFixed(2);
            }

            return convPrice;
        }

        render() {
            return (
                <ProductComponent convertedPrice={this.convertPrice()} {...this.props} />
            )
        }
    }
};

export default productComponent;