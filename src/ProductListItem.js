import React, { Component } from "react";
import productComponent from './ProductComponent.js';
import { Link } from 'react-router-dom'
import './ProductListItem.css';

class ProductListItem extends Component {

    render() {
        return (
            <div className="ProductListItem">
                <div><span className="ProductListItem_Label">Name:</span> {this.props.product.name}</div>
                <div><span className="ProductListItem_Label">Price:</span> ${this.props.convertedPrice} {this.props.selectedCurrency.base}</div>
                <div><Link to={"/product/" + this.props.product.id}>Details</Link></div>
            </div>
        );
    }
}

export default productComponent(ProductListItem);