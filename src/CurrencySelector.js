import React, { Component } from 'react';

class CurrencySelector extends Component {
    render() {
        const items = this.props.currencies.map((v) => {
            return (<option key={v.base} value={v.base}>{v.base}</option>)
        });

        return (
            <select value={this.props.selectedCurrency.base} onChange={this.props.changeCurrency} disabled={this.props.disabled}>
                {items}
            </select>
        )
    }
}

export default CurrencySelector;