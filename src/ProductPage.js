import React, { Component } from "react";
import ProductList from "./ProductList.js";
import ProductItem from "./ProductItem.js";
import "./ProductPage.css";

class ProductPage extends Component {

    /**
     * Get the related products
     * @param {any} product
     */
    relatedProducts(product) {
        return this.props.products.filter((v) => {
            return product.relatedProducts.includes(parseInt(v.id));
        });
    }

    render() {
        const product = this.props.products.filter((v) => v.id == parseInt(this.props.match.params.id));

        if (product.length === 0) {
            return (<span>Product not found</span>);
        }

        let error = { display: 'none' };
        if (this.props.error !== "") {
            error.display = 'block';
        }

        return (
            <div className="ProductPage">
                <h1>Product</h1>
                <div className="ProductPage_Error" style={error}>{this.props.error}</div>
                <ProductItem {...this.props} product={product[0]} />
                <hr/>
                <h3>Related:</h3>
                <ProductList {...this.props} products={this.relatedProducts(product[0])} />
            </div>
        )
    }
}

export default ProductPage;