import React, { Component } from 'react';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom'
import Navigation from './Navigation.js';
import CurrencySelector from './CurrencySelector';
import ProductList from './ProductList.js';
import ProductPage from './ProductPage.js';
import rates from "./data/exchange_rates.json";
import products from "./data/products.json";

/**
 * Class App
 */
class App extends Component {

    /**
     * 
     * @param {any} props
     */
    constructor(props) {
        super(props);
        this.state = {
            selectedCurrency: rates[0],
            error: "",
            products: products
        };
        this.changeCurrency = this.changeCurrency.bind(this);
        this.updateProduct = this.updateProduct.bind(this);
    }

    /**
     * Manage a currency change
     * @param SyntheticEvent e
     */
    changeCurrency(e) {
        e.preventDefault();
        this.setState({
            selectedCurrency: rates.filter((v) => v.base === e.target.value)[0]
        });
    }

    /**
     * Update the product in our store
     * @param {any} product
     * @param {any} oldProductId
     */
    updateProduct(product, oldProductId) {
        if (!this.validateProduct(product, oldProductId)) {
            this.setState({ error: "Validation Failed" });
            //clear the error after 5 secs
            setTimeout((() => { this.setState({ error: "" }); }).bind(this), 5000);
            return false;
        }

        this.setState({ error: "" });

        //find the product
        const index = products.findIndex((v) => v.id == oldProductId);


        //update
        if (index >= 0) {
            let products = this.state.products;
            products[index] = product;
            this.setState({
                products: products
            });
        }

        return true;
    }

    /**
     * Perform validation on the product
     * @param {any} product
     * @param {any} oldProductId
     */
    validateProduct(product, oldProductId) {
        if (product.id != oldProductId && products.find((v) => v.id == product.id)) {
            return false;
        }

        if (product.name.length <= 3) {
            return false;
        }

        return true;
    }

    /**
     * Render
     * */
    render() {
        const left = (
            <Link to="/">Home</Link>
        );

        const right = (
            <CurrencySelector
                currencies={rates}
                selectedCurrency={this.state.selectedCurrency}
                changeCurrency={this.changeCurrency} />
        );

        return (
            <Router>
                <div>
                    <Navigation
                        left={left}
                        right={right}
                    />
                    <Route exact path="/" render={(props) => {
                        return (
                            <ProductList
                                currencies={rates}
                                selectedCurrency={this.state.selectedCurrency}
                                products={this.state.products}
                                {...props}
                            />
                        );
                    }} />
                    <Route path="/product/:id" render={(props) => {
                        return (
                            <ProductPage
                                currencies={rates}
                                selectedCurrency={this.state.selectedCurrency}
                                products={this.state.products}
                                updateProduct={this.updateProduct}
                                error={this.state.error}
                                {...props}
                            />
                        );
                    }} />
                </div>
            </Router>
        );
    }
}

export default App;
