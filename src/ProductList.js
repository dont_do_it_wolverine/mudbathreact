import React, { Component } from "react";
import ProductListItem from './ProductListItem.js';
import './ProductList.css';

class ProductList extends Component {
    render() {
        const products = this.props.products.map((v) => {
            return (
                <ProductListItem key={v.id} product={v} currencies={this.props.currencies} selectedCurrency={this.props.selectedCurrency} />
            );
        });

        return (
            <div className="ProductList">{products}</div>
        );
    }
}

export default ProductList;