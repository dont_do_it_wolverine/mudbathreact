import React, { Component } from "react";
import productComponent from './ProductComponent.js';
import CurrencySelector from './CurrencySelector';
import "./ProductItem.css";


class ProductItem extends Component {
    constructor(props) {
        super(props);

        this.state = {
            editModeEnabled: false,
            product: {
                ...this.props.product,
                price: {
                    amount: this.props.convertedPrice,
                    base: this.props.selectedCurrency.base
                }
            },
            currency: this.props.selectedCurrency,
            convertedPrice: this.props.convertedPrice
        };
       
        this.toggleEditMode = this.toggleEditMode.bind(this);
        this.onFieldChange = this.onFieldChange.bind(this);
        this.onCurrencyChange = this.onCurrencyChange.bind(this);
        this.onRelatedChange = this.onRelatedChange.bind(this);
        this.onPriceChange = this.onPriceChange.bind(this);
    }

    toggleEditMode(e) {
        e.preventDefault();

        if (this.state.editModeEnabled && !this.props.updateProduct(this.state.product, this.props.product.id)) {
            return;
        }

        this.setState({editModeEnabled: !this.state.editModeEnabled});
    }

    onFieldChange(e) {
        this.setState({
            product: {
                ...this.state.product,
                [e.target.name]: e.target.value
            }
        });
    }

    onCurrencyChange(e) {
        this.setState({
            product: {
                ...this.state.product,
                price: {
                    base: e.target.value,
                    amount: this.state.product.price.amount
                }
            },
            currency: e.target.value
        })
    }

    onPriceChange(e) {
        this.setState({
            product: {
                ...this.state.product,
                price: {
                    base: this.state.product.price.base,
                    amount: e.target.value
                }
            },
            convertedPrice: e.target.value
        })
    }

    onRelatedChange(e) {
        this.setState({
            product: {
                ...this.state.product,
                relatedProducts: [].slice.call(e.target.selectedOptions).map((v) => parseInt(v.value))
            }
        });
    }

    /**
     * Router doesn't create a new ProductItem component when the props change, it just updates them so if the id changes
     * we need to manually update the state
     * @param {any} prevProps
     * @param {any} prevState
     */
    componentDidUpdate(prevProps, prevState) {
        if (prevProps.product.id != this.props.product.id || prevProps.selectedCurrency != this.props.selectedCurrency) {    
            this.setState({
                editModeEnabled: false,
                product: {
                    ...this.props.product,
                    price: {
                        amount: this.props.convertedPrice,
                        base: this.props.selectedCurrency.base
                    }
                },
                currency: this.props.selectedCurrency,
                convertedPrice: this.props.convertedPrice
            });
        }
    }

    render() {
        const options = this.props.products.map((v) => {
            return (
                <option key={v.id} value={v.id} >{v.id}: {v.name}</option>
            );
        });

        const editText = this.state.editModeEnabled ? 'Save' : 'Edit';

        return (
            <div key={this.state.product.id}>
                <div className="ProductItem">
                    <div className="ProductItem_FormRow">
                        <label className="strong">ID: </label>
                        <input type="text" defaultValue={this.state.product.id} disabled={!this.state.editModeEnabled} name="id" onChange={this.onFieldChange}/>
                    </div>
                    <div className="ProductItem_FormRow">
                        <label className="strong">Name: </label>
                        <input type="text" defaultValue={this.state.product.name} disabled={!this.state.editModeEnabled} name="name" onChange={this.onFieldChange} />
                    </div>
                    <div className="ProductItem_FormRow">
                        <label className="strong">Description:</label>
                        <input type="text" defaultValue={this.state.product.description} disabled={!this.state.editModeEnabled} name="description" onChange={this.onFieldChange} />
                    </div>
                    <div className="ProductItem_FormRow">
                        <label className="strong">Price: </label>
                        <div className="ProductItem_FormRowInputGroup">
                            <input type="text" value={this.state.convertedPrice} disabled={!this.state.editModeEnabled} name="price" onChange={this.onPriceChange} />
                            <CurrencySelector
                                currencies={this.props.currencies}
                                selectedCurrency={this.state.currency}
                                changeCurrency={this.onCurrencyChange}
                                disabled={!this.state.editModeEnabled}
                            />
                        </div>
                    </div>
                    <div className="ProductItem_FormRow" style={{display: this.state.editModeEnabled ? 'flex' : 'none'}}>
                        <label className="strong">Related: </label>
                        <select multiple defaultValue={this.state.product.relatedProducts} onChange={this.onRelatedChange}>
                            {options}
                        </select>
                    </div>
                    <a href="/" onClick={this.toggleEditMode}>{editText}</a>
                </div>
            </div>
        )
    }
}

export default productComponent(ProductItem);